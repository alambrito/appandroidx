package com.example.appmllf

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class detailActivity : AppCompatActivity() {

    private lateinit var database: AppDatabase
    private lateinit var repertorio: Repertorio
    private lateinit var repertorioLiveData: LiveData<Repertorio>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        database = AppDatabase.getDatabase(this)

        val idR = intent.getIntExtra("id", 0)

        repertorioLiveData = database.playlist().get(idR)

        repertorioLiveData.observe(this, Observer {
            repertorio = it
            titleR.text = repertorio.nombre
            notasR.text = "[ ${repertorio.notasGuia} ]"
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.repositoriomenu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.delete_item -> {
                //eliminaremos todos los observadores de nuestras musicas para poder eliminar de manera segura
                repertorioLiveData.removeObservers(this)
                CoroutineScope(Dispatchers.IO).launch {
                    database.playlist().delete(repertorio)
                    this@detailActivity.finish()
                }
                val text = "Eliminado con éxito"
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(applicationContext, text, duration)
                toast.show()

            }
        }

        return super.onOptionsItemSelected(item)
    }
}