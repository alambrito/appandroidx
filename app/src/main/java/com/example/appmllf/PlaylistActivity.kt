package com.example.appmllf

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_playlist.*

class PlaylistActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist)

        var listRepertorio = emptyList<Repertorio>()

        //pasamos la base de datos
        val database = AppDatabase.getDatabase(this)

        database.playlist().getAll().observe(this, Observer {
            listRepertorio = it

            val adapter = RepertorioAdapter(this, listRepertorio)

            list_repertorio.adapter = adapter
        })

        list_repertorio.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, posicion: Int, l: Long ->
            val intent = Intent(this, detailActivity::class.java)
            intent.putExtra("id", listRepertorio[posicion].idmusic)
            startActivity(intent)
        }

    }

}