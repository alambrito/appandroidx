package com.example.appmllf

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Music::class, Repertorio::class], version = 3)
abstract class AppDatabase : RoomDatabase() {

    abstract fun musics(): MusicDAO
    abstract fun playlist(): PlaylistDAO

    companion object {
        // check si la instancia ya esta creada
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE

            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "app_database"
                ).build()

                INSTANCE = instance

                return instance

            }

        }
    }


}