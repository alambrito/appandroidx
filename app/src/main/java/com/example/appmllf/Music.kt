package com.example.appmllf

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "musics")
class Music(
    val nombre: String,
    val autor: String,
    val album: String,
    val vol: Int,
    val notas: String,
    val logoM: Int,
    @PrimaryKey(autoGenerate = true)
    var idmusic: Int = 0
) : Serializable {
}

@Entity(tableName = "playlist")
class Repertorio(
    val nombre: String,
    val notasGuia: String,
    @PrimaryKey(autoGenerate = true)
    var idmusic: Int = 0
) : Serializable {
}



