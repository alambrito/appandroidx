package com.example.appmllf

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_agregar_music.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AgregarMusic : AppCompatActivity() {

    private val SELECT_ACTIVITY = 40
    private var imageUri: Uri? = null //instanciamos preparador de select de imagen

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_music)

        //mensajes
        var text: String = ""
        var duration = Toast.LENGTH_SHORT
        //para poder editar
        var idmusic: Int? = null

        if (intent.hasExtra("music")) {
            val music = intent.extras?.getSerializable("music") as Music

            tituloc.setText(music.nombre)
            autc.setText(music.autor)
            albumc.setText(music.album)
            volc.setText(music.vol.toString())
            notasc.setText(music.notas)
            idmusic = music.idmusic

            //si nos biene un producto
            val imageUri = ImageController.getImageUri(this, idmusic.toLong())
            imageSelect_iv.setImageURI(imageUri)
        }


        //instanciamos la db
        val database = AppDatabase.getDatabase(this)

        buttonAdd.setOnClickListener {
            val titulo = tituloc.text.toString()
            val autor = autc.text.toString()
            val album = albumc.text.toString()
            val vol = volc.text.toString().toInt()
            val descripcion = notasc.text.toString()

            //creamos nuestro insert
            val repertorio = Music(titulo, autor, album, vol, descripcion, R.drawable.logo)

            if (repertorio != null) {

                //preparativos para insertar lo modificado
                if (idmusic != null) {
                    CoroutineScope(Dispatchers.IO).launch {
                        repertorio.idmusic = idmusic

                        database.musics().update(repertorio)

                        //agregamos nuestra imagen seleeciona a la app -> esto al momento de actualizar
                        imageUri?.let {
                            ImageController.saveImage(this@AgregarMusic, idmusic.toLong(), it)
                        }

                        this@AgregarMusic.finish()


                    }
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    text = "Actualizado con éxito"
                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.show()
                } else {

                    //en caso que el anterior sea nulo, pasa normalmenta a insert
                    //agregamos una corruptiva por no saber el tiempo de ejecucion o de latencia

                    CoroutineScope(Dispatchers.IO).launch {
                        //hacemos el llamado a la base de datos
                        val id = database.musics().inserAll(repertorio)[0]

                        //agregamos nuestra imagen seleeciona a la app -> esto al momento de actualizar
                        imageUri?.let {
                            ImageController.saveImage(this@AgregarMusic, id, it)
                        }

                        this@AgregarMusic.finish()
                    }
                    text = "Agregado con éxito"
                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.show()
                }
            } else {
                //this@AgregarMusic.finish()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }

        //onclick para seleccionar la imagen
        imageSelect_iv.setOnClickListener {
            ImageController.selectPhotoFromGallery(this, SELECT_ACTIVITY)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when {
            requestCode == SELECT_ACTIVITY && resultCode == Activity.RESULT_OK -> {
                imageUri = data!!.data //decimos que la data no es null
                imageSelect_iv.setImageURI(imageUri)
            }
        }

    }
}