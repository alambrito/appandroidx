package com.example.appmllf

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_music.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MusicActivity : AppCompatActivity() {
    //val button = findViewById<Button>(R.id.button)
    private lateinit var database: AppDatabase
    private lateinit var music: Music
    private lateinit var musicLiveData: LiveData<Music>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music)



        database = AppDatabase.getDatabase(this)

        val idM = intent.getIntExtra("id", 0)

        val imageUri = ImageController.getImageUri(this, idM.toLong())


        musicLiveData = database.musics().get(idM)

        musicLiveData.observe(this, Observer {
            music = it

            nombrec.text = music.nombre
            autor.text = "Autor : ${music.autor}"
            albumc.text = "Album : ${music.album}"
            volumen.text = "Volumen: ${music.vol}"
            notasc.text = "Notas: ${music.notas}"
            imgNotas.setImageURI(imageUri)
            //imgNotas.setImageResource(music.logoM)
        })

        ///////////////////////////////////////////////////////////////////////////////
        //agrega un elemento al repertorio
        //database = AppDatabase.getDatabase(this)

        findViewById<Button>(R.id.btnAddR).setOnClickListener {
            //instanciamos el valor que requerimos
            val titulo = nombrec.text.toString()
            val notas = notasc.text.toString()
            val rep = Repertorio(titulo, notas)

            System.out.println("HERE")
            // build alert dialog
            val dialogBuilder = AlertDialog.Builder(this)

            // set message of alert dialog
            dialogBuilder.setMessage("¿Segur@ que deseas agrgarla al repertorio?")
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("Si", DialogInterface.OnClickListener { dialog, id ->
                    finish()
                    CoroutineScope(Dispatchers.IO).launch {
                        database.playlist()
                            .inserAll(rep)//aqui agregamos al repertorio la cancion seleccionada
                        this@MusicActivity.finish()
                    }

                    val text = "Canción agregada con exito"
                    val duration = Toast.LENGTH_SHORT

                    val text2 = "Visualiza tu repertorio"
                    val duration2 = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(applicationContext, text, duration)
                    val toast2 = Toast.makeText(applicationContext, text2, duration2)
                    toast.show()
                    toast2.show()
                })
                // negative button text and action
                .setNegativeButton("No", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })

            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("MENSAJE DE CONFIRMACIÓN")
            // show alert dialog
            alert.show()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.musicmenu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.edit_item -> {
                val intent = Intent(this, AgregarMusic::class.java)
                intent.putExtra("music", music)
                startActivity(intent)
            }

            R.id.delete_item -> {
                //eliminaremos todos los observadores de nuestras musicas para poder eliminar de manera segura
                musicLiveData.removeObservers(this)
                CoroutineScope(Dispatchers.IO).launch {
                    database.musics().delete(music)
                    this@MusicActivity.finish()
                }
                val text = "Canción eliminada con éxito"
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(applicationContext, text, duration)
                toast.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}