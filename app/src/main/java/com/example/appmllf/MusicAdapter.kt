package com.example.appmllf

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.item_music.view.*


class MusicAdapter(private val mContext: Context, private val listamusic: List<Music>) :
    ArrayAdapter<Music>(mContext, 0, listamusic) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        //instanciamos el layout
        val layout = LayoutInflater.from(mContext).inflate(R.layout.item_music, parent, false)

        //pasmamos la inf que tenemos en nuestra lista

        val music = listamusic[position]
        System.out.println(music)
        layout.nombrec.text = music.nombre
        layout.autc.text = "Autor :${music.autor}"
        val imageUri = ImageController.getImageUri(mContext, music.idmusic.toLong())
        layout.imageView.setImageURI(imageUri)


        return layout
    }
}