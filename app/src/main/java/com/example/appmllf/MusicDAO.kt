package com.example.appmllf

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MusicDAO {
    @Query("SELECT * FROM musics") // eleccionamos todos los datos de mi base de datos
    fun getAll(): LiveData<List<Music>> //mostramos todos los datos en vivo, con un observable

    @Query("SELECT * FROM musics WHERE idmusic = :id")
    fun get(id: Int): LiveData<Music>

    @Insert
    fun inserAll(vararg music: Music): List<Long>

    @Update
    fun update(music: Music)

    @Delete
    fun delete(music: Music)
}

@Dao
interface PlaylistDAO {
    @Query("SELECT * FROM playlist") // eleccionamos todos los datos de mi base de datos
    fun getAll(): LiveData<List<Repertorio>> //mostramos todos los datos en vivo, con un observable

    @Query("SELECT * FROM playlist WHERE idmusic = :id")
    fun get(id: Int): LiveData<Repertorio>

    @Insert
    fun inserAll(vararg repertorio: Repertorio)

    @Update
    fun update(repertorio: Repertorio)

    @Delete
    fun delete(repertorio: Repertorio)
}
