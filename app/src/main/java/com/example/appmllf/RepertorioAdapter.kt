package com.example.appmllf

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.item_playlist.view.*

class RepertorioAdapter(
    private val mContext: Context,
    private val listaRepertorio: List<Repertorio>
) : ArrayAdapter<Repertorio>(mContext, 0, listaRepertorio) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.item_playlist, parent, false)

        val playlist = listaRepertorio[position]
        System.out.println(playlist.nombre.toString())
        layout.title.text = playlist.nombre
        layout.notas_g.text = playlist.notasGuia


        return layout
    }
}