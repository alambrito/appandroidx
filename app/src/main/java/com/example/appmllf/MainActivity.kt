package com.example.appmllf

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var listamusica = emptyList<Music>()

        val database = AppDatabase.getDatabase(this)

        database.musics().getAll().observe(this, Observer {
            listamusica = it
            val adapter = MusicAdapter(this, listamusica)

            list.adapter = adapter
        })



        list.setOnItemClickListener { adapterView, view, posicion, id ->
            val intent = Intent(this, MusicActivity::class.java)
            intent.putExtra("id", listamusica[posicion].idmusic)
            startActivity(intent)
        }

        floatingActionButton.setOnClickListener {
            val intent = Intent(this, AgregarMusic::class.java)
            startActivity(intent)
        }


        btn_listview_rep.setOnClickListener {
            val intent = Intent(this, PlaylistActivity::class.java)
            startActivity(intent)
        }


    }



}